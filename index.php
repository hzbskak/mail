<?php
#!/bin/bash

/**
 * User: hzbskak
 * Date: 2019/12/25 - 14:54
 */

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
require 'vendor/autoload.php';
class Email{
    public static function sendEmail($email,$body){
        $mail = new PHPMailer(true);
        try {

            //使用STMP服务
            $mail->isSMTP();

            //这里使用我们第二步设置的stmp服务地址
            $mail->Host = "smtp.sina.com";

            //设置是否进行权限校验
            $mail->SMTPAuth = true;

            //第二步中登录网易邮箱的账号
            $mail->Username = "hzbskak@sina.com";

            //客户端授权密码，注意不是登录密码
            $mail->Password = "";

            //使用ssl协议
            $mail->SMTPSecure = 'ssl';

            //端口设置
            $mail->Port = 465;

            //字符集设置，防止中文乱码
            $mail->CharSet= "utf-8";

            //设置邮箱的来源，邮箱与$mail->Username一致
            $mail->setFrom("hzbskak@sina.com", "测试来源");

            //设置收件的邮箱地址
            $mail->addAddress($email);

            //设置回复地址，一般与来源一致
            $mail->addReplyTo("hzbskak@sina.com", "测试来源");

            $mail->isHTML(true);
            //标题
            $mail->Subject = '测试标题';
            //正文
            $mail->Body    = $body;
            $mail->send();
            echo 'ok';
            return true;
        } catch (Exception $e) {
            var_dump( array('errCode'=>-1,'msg'=>$mail->ErrorInfo));
        }
    }
}
Email::sendEmail('13180449192@sina.cn', '测试内容体');

